#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <list>
#include <algorithm>
#include <queue>
#include <iostream>
#include <windows.h>
#include <fstream>
using namespace std;

class graf
{
public:
	virtual int wierzcholki() = 0;
	virtual void generuj(int ilosc_wierzcholkow, int gestosc)=0;


	class ItImpl
	{
	public:
		virtual ItImpl* clone() = 0;
		virtual void inkrementacja() = 0;
		virtual bool isend() = 0;
		virtual int wartosc() = 0;
		virtual int sasiad() = 0;
	};




	class iterator
	{
	private:
		ItImpl* impl;
	public:
		iterator() : impl() {}
		iterator(ItImpl* impl) : impl(impl) {}
		iterator(const iterator &i) : impl(i.impl->clone()) {} // Konstruktor kopiujacy


		iterator& operator=(const iterator &i)
		{

			impl = i.impl->clone();
			return *this;
		}

		void nastepny()
		{
			impl->inkrementacja();
		}

		bool isend()
		{
			return impl->isend();
		}

		int wartosc()
		{
			return impl->wartosc();
		}

		int sasiad()
		{
			return impl->sasiad();
		}
	};
	virtual iterator poczatek(int v) = 0;

	virtual int krawedz(int wierz1, int wierz2) = 0;
};

bool spojnosc(graf* G)
{
	int W = G->wierzcholki();
	int *O = new int[W]; // odwiedzone
	int *K = new int[W]; // kolejka do ktorej nie dodamy wiecej niz K wierzcholkow;
	int poczatek = 0; // poczatek kolejki
	int koniec = 0; // koniec kolejki

	for (int i = 0; i < W; i++)
		O[i] = 0; // jeszcze nic nie odwiedzilismy

				  // dodajemy wierzcholek 0 do kolejki
	K[koniec] = 0;
	koniec++;
	O[0] = 1;

	while (poczatek < koniec)
	{
		// sciagamy element z kolejki
		int aktualny = K[poczatek];
		poczatek++;

		// przegladamy wszystkich jego sasiadow
		for (graf::iterator it = G->poczatek(aktualny); !it.isend(); it.nastepny())
		{
			if (O[it.sasiad()] == 0)
			{
				K[koniec] = it.sasiad();
				koniec++;
				O[it.sasiad()] = 1;
			}
		}
	}

	return (koniec == W); // jesli odwiedzilismy wszystkie wierzcholki to graf byl spojny
}

struct krawedz // krawedz - do algorytmu kruskala
{
	int u, v, w; // Z wierzcholka u do wierzcholka v o wadze w
	krawedz() {}
	krawedz(int u, int v, int w) : u(u), v(v), w(w) {}
};

// operator przyda sie do funkcji sort
bool operator<(const krawedz &a, const krawedz &b)
{
	return a.w < b.w;
}

// Union-Find
class UF
{
private:
	int *T;
public:
	UF(int w)
	{
		T = new int[w];
		for (int i = 0; i < w; i++)
			T[i] = i;
	}



	int find(int w)
	{
		if (T[w] != w)
			T[w] = find(T[w]);
		return T[w];
	}

	// nazwa union w C++ jest zarezerwowana do czego innego
	bool unia(int u, int v)
	{
		int fu = find(u);
		int fv = find(v);

		if (fu != fv)
		{
			T[fu] = fv;
			return true;
		}
		return false;
	}
};

// Graf macierz
class graf_macierz : public graf
{
private:
	int **M; //  macierz sasiedztwa
	int W; // ilosc Wierzcholkow

	class ItMacierz : public ItImpl
	{
	private:
		int kolumna;
		int index;
		graf_macierz* owner;
	public:
		ItMacierz(graf_macierz* own, int k) : owner(own), kolumna(k), index(-1) {}
		ItMacierz(graf_macierz* own, int k, int i) : owner(own), kolumna(k), index(i) {}
		ItMacierz* clone()
		{
			return new ItMacierz(owner, kolumna, index);
		}
		void inkrementacja()
		{
			index++;
			while (index < owner->W && owner->M[kolumna][index] == -1)
				index++;
		}
		bool isend()
		{
			return (index == owner->W);
		}
		int wartosc()
		{
			return owner->M[kolumna][index];
		}
		int sasiad()
		{
			return index;
		}
	};
public:
	graf_macierz();
	~graf_macierz();

	int wierzcholki();

	void generuj(int ilosc_wierzcholkow, int gestosc);

	bool zapisz(char* nazwa_pliku);

	iterator poczatek(int v)
	{
		ItImpl* it = new ItMacierz(this, v);
		it->inkrementacja();
		return iterator(it);
	}

	int krawedz(int wierz1, int wierz2);
};

graf_macierz::graf_macierz()
{
	M = NULL;
	W = 0;
}

graf_macierz::~graf_macierz()
{
	if (M != NULL)
	{
		for (int i = 0; i < W; i++)
			delete[] M[i];                       //usuwanie wierszy tablic
			delete[] M;
	}
}

int graf_macierz::wierzcholki()
{
	return W;
}

void graf_macierz::generuj(int ilosc_wierzcholkow, int gestosc)
{
	W = ilosc_wierzcholkow;

	if (M != NULL)
	{
		for (int i = 0; i < W; i++)
			delete[] M[i];
		delete[] M;
	}
	M = new int*[W];
	for (int i = 0; i < W; i++)
		M[i] = new int[W];

	do
	{
		for (int i = 0; i < W; i++)
			for (int j = 0; j < W; j++)
				M[i][j] = -1;

		int maks_krawedzi = W * (W - 1) / 2;

		for (int k = 0; k < gestosc * maks_krawedzi / 100; k++)
		{
			int i = rand() % W;
			int j = rand() % W;

			if (i == j || M[i][j] != -1)
			{
				k--;
			}
			else
			{
				M[i][j] = M[j][i] = rand() % 11; // waga 0 - 10
			}
		}
	} while (!spojnosc(this));


}

int graf_macierz::krawedz(int wierz1, int wierz2)
{
	return M[wierz1][wierz2];
}




// Graf lista

class graf_lista : public graf
{
private:
	list<pair<int, int> > *L; // lista sasiedztwa inty to wierzcholki, drugie inty to wagi
	int W; // ilosc Wierzcholkow

	class ItList : public ItImpl
	{
	private:
		list<pair<int, int> >::iterator it;
		graf_lista *owner;
		int kolumna;
	public:
		ItList(graf_lista* gl, int k) : owner(gl), kolumna(k), it(gl->L[k].begin()) {}
		ItList(graf_lista* gl, int k, list<pair<int, int> >::iterator it) : owner(gl), kolumna(k), it(it) {}
		ItList* clone()
		{
			return new ItList(owner, kolumna, it);
		}
		void inkrementacja()
		{
			it++;
		}
		bool isend()
		{
			return (it == owner->L[kolumna].end());
		}
		int wartosc()
		{
			return (*it).second;
		}
		int sasiad()
		{
			return (*it).first;
		}

	};
public:
	graf_lista();
	~graf_lista();

	int wierzcholki();

	void generuj(int ilosc_wierzcholkow, int gestosc);


	iterator poczatek(int v)
	{
		ItList* it = new ItList(this, v);
		return iterator(it);
	}

	int krawedz(int w1, int w2);
};

graf_lista::graf_lista()
{
	L = NULL;
	W = 0;
}

graf_lista::~graf_lista()
{
	if (L != NULL)
		delete[] L;
}

int graf_lista::wierzcholki()
{
	return W;
}

void graf_lista::generuj(int ilosc_wierzcholkow, int gestosc)
{
	W = ilosc_wierzcholkow;

	if (L != NULL)
		delete[] L;
	L = new list<pair<int, int> >[W];

	do
	{
		for (int i = 0; i < W; i++)
			L[i].clear();

		int maks_krawedzi = W * (W - 1) / 2;

		for (int k = 0; k < gestosc * maks_krawedzi / 100; k++)
		{
			int i = rand() % W;
			int j = rand() % W;

			if (i == j || krawedz(i, j) != -1)
			{
				k--;
			}
			else
			{
				int w = rand() % 11;
				L[i].push_back(make_pair(j, w));
				L[j].push_back(make_pair(i, w));
			}
		}
	} while (!spojnosc(this));
}

int graf_lista::krawedz(int w1, int w2)
{
	for (list<pair<int, int> >::iterator it = L[w1].begin(); it != L[w1].end(); it++)
		if ((*it).first == w2)
			return (*it).second;
	return -1;
}



// mierznie czasu
double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
double GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}

// Algorytm Prima
int Prim(graf* G)
{
	int wynik = 0;
	int* czy_w_drzewie = new int[G->wierzcholki()];
	priority_queue<krawedz> PQ;

	for (int i = 0; i < G->wierzcholki(); i++)
		czy_w_drzewie[i] = 0;

	// wstawiamy 0 do drzewa;
	czy_w_drzewie[0] = 1;
	for (graf::iterator it = G->poczatek(0); !it.isend(); it.nastepny())
	{
		krawedz k(0, it.sasiad(), it.wartosc());

		PQ.push(k);
	}

	while (!PQ.empty())
	{
		krawedz akt = PQ.top();
		PQ.pop();

		if (czy_w_drzewie[akt.v] == 0)
		{
			wynik += akt.w;
			for (graf::iterator it = G->poczatek(akt.v); !it.isend(); it.nastepny())
			{
				krawedz k(akt.v, it.sasiad(), it.wartosc());

				PQ.push(k);
			}
			czy_w_drzewie[akt.v] = 1;
		}
	}

	return wynik;
}


// Algorytm Kruskala
int Kruskal(graf* G)
{
	int max_krawedzi = G->wierzcholki() * G->wierzcholki();
	krawedz *K = new krawedz[max_krawedzi];
	int ile_krawedzi = 0;

	// Zbieramy wszystkie krawedzie i zapisujemy do tablicy K
	for (int i = 0; i < G->wierzcholki(); i++)
	{
		for (graf::iterator it = G->poczatek(i); !it.isend(); it.nastepny())
		{
			K[ile_krawedzi] = krawedz(i, it.sasiad(), it.wartosc());
			ile_krawedzi++;
		}
	}

	// sortujemy K po wagach
	sort(K, K + ile_krawedzi);

	int wynik = 0;
	UF uf(G->wierzcholki());

	for (int i = 0; i < ile_krawedzi; i++)
	{

		if (uf.unia(K[i].u, K[i].v))
			wynik += K[i].w;
	}

	return wynik;
}





int main()
{
fstream plik;
plik.open("pomiary100-100.txt", ios::out);
plik << "macierz" << "\n";
plik << "Prim" << "\n";
	for (int t = 0; t < 100; t++)
	{
		graf_macierz N;
		N.generuj(100, 100);
		 StartCounter();
		 Prim(&N);
		 plik << GetCounter() << "\n";
	}
	plik << "Kruskal" << "\n";
	for (int t = 0; t < 100; t++)
	{
		graf_macierz N;
		N.generuj(100, 100);
		StartCounter();
		Kruskal(&N);
		plik << GetCounter() << "\n";
	}
	cout << "jestem w polowie" << "\n";
	plik << "lista" << "\n";
	plik << "Prim" << "\n";
	for (int t = 0; t < 100; t++)
	{
		graf_lista M;
		M.generuj(100, 100);
		StartCounter();
		Prim(&M);
		plik << GetCounter() << "\n";
	}
	plik << "Kruskal" << "\n";
	for (int t = 0; t < 100; t++)
	{
		graf_lista M;
		M.generuj(100, 100);
		StartCounter();
		Kruskal(&M);
		plik << GetCounter() << "\n";
	}


	plik.close();
	system("pause");
	return 0;
}



